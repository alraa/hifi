$(document).ready(function() {
	//placeholder
	$('[placeholder]').focus(function() {
		var input = $(this);
		if(input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if(input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if(input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});
	
	//
	$('select, input[type="checkbox"], input[type="radio"]').styler();
	
	//
	$('.slider-top').slick({
	 	dots: true,
	 	speed: 200,
		autoplay:  false,
		autoplaySpeed: 4000,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint:1024,
		  settings: {
			arrows:false
		  }
		}
	  ]
	});

	$('.slider-piq').slick({
	 	speed: 200,
		infinite: false,
		autoplay:  false,
		autoplaySpeed: 4000,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth:false,
		responsive: [
		{
		  breakpoint:1024,
		  settings: {
			slidesToShow:3,
			variableWidth:true,
			slidesToScroll: 1,
			infinite:false
		  }
		},{
		  breakpoint:600,
		  settings: {
			slidesToShow:1,
			variableWidth:false,
			slidesToScroll: 1,
			infinite:true
		  }
		}
	  ]
	});

	$('.js-tovs').slick({
	 	speed: 200,
		autoplay:  false,
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint:1024,
		  settings: {
			slidesToShow:3,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		},
		{
		  breakpoint:768,
		  settings: {
			slidesToShow:2,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		}
	  ]
	});

	$('.js-brands').slick({
	 	speed: 200,
		autoplay:  false,
		autoplaySpeed: 4000,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint:1110,
		  settings: {
			slidesToShow:5,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		},{
		  breakpoint:1024,
		  settings: {
			slidesToShow:4,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		},{
		  breakpoint:800,
		  settings: {
			slidesToShow:3,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		},
		{
		  breakpoint:640,
		  settings: {
			slidesToShow:2,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		}
	  ]
	});

	$('.js-news').slick({
	 	speed: 200,
		autoplay:  false,
		autoplaySpeed: 4000,
		slidesToShow: 5,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint:1024,
		  settings: {
			slidesToShow:4,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		},{
		  breakpoint:800,
		  settings: {
			slidesToShow:3,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		},
		{
		  breakpoint:640,
		  settings: {
			slidesToShow:2,
			slidesToScroll: 1,
			arrows:false,
			dots: true
		  }
		}
	  ]
	});

	//
	var ww = document.body.clientWidth;
	var wh = document.body.clientHeight;
	$(document).ready(function() {
		adjustMenu();
	});
	$(window).bind('resize orientationchange', function() {
		ww = document.body.clientWidth;
		wh = $('body').height();
		adjustMenu();
	});
	var adjustMenu = function() {
		if(ww < 768) {			
			
		}
		else if(ww >= 768) {
			
		}

		var offs = $('.cats').offset().top
		$('.cats').css('max-height', wh - 60);
	}
	
	//
	$('.mobile-button').click(function() {			
		$(this).toggleClass('open');
		$('#header').toggleClass('opened');
		return false;
	});

	$('.abs .labl').click(function() {			
		$(this).parent().find('.hint').fadeIn(300);
		return false;
	});
	$('.hint .close').click(function() {			
		$(this).parent('.hint').fadeOut(300);
		return false;
	});

	$('.piqs .slick-slide').click(function() {			
		$(this).toggleClass('open');
		$(this).siblings().removeClass('open');
		return false;
	});

	$('.foot-opener').click(function() {			
		$(this).next('.foot-nav').toggleClass('open');
		$(this).toggleClass('open');
		return false;
	});

	$('.filter-title').click(function() {			
		$(this).parent('.filter-item').toggleClass('open');
		return false;
	});



	//fixed header
	function showDiv() {
		if ($(window).scrollTop() > 230 && $('#header').data('positioned') == 'false') {
			$("#header").data('positioned', 'true');
			$("#header").addClass('fix');
		}else if ($(window).scrollTop() <= 230 && $('#header').data('positioned') == 'true') {
			$("#header").fadeIn(0, function() {
				$("#header").removeClass('fix');
			}).data('positioned', 'false');
		}
	}
	$(window).scroll(showDiv);
	//$(window).load(showDiv);
	$('#header').data('positioned', 'false');

	//
	$('.fancy').fancybox();
	
	//
	var maxHeight = -1;
	$('.cats .drop ul').each(function() {
		maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });	
    $('.cats > ul').css({'min-height':maxHeight + 450});
    $('.cats .drop').hide();
    $('.cats').addClass('hid');
	
	$(".head-cats").mouseover(function() {
		$('#header').addClass('open');
	    $('#mainafter').addClass('open');
		$('.cats').removeClass('hid');
	}).mouseout(function() {
		$('#header').removeClass('open');
        $('#mainafter').removeClass('open');
		$('.cats').addClass('hid');
	});
	
    $('.cats li').hover(function(){     
	    $(this).addClass('active');
	    $(this).siblings().removeClass('active');
	});

    $('.filt-btn').click(function(){     
        $('body').toggleClass('hid2');
        $('.filter-side').toggleClass('open');
        return false;
    });
    $('#mainafter2').click(function(){     
        $('body').removeClass('hid2');
        $('.filter-side').removeClass('open');
    });

	
	// 
	$('.pass-inp').focus(function(){     
	    $(this).attr('type', 'password'); 
	});
	$('.pass-inp').change(function(){     
	    if( $(this).val().length==0 ){
		    $(this).attr('type', 'text');
		}
		else{
		    $(this).attr('type', 'password'); 
			$('.eye input').change(function() {
			  var isChecked = $(this).prop('checked');
			  if (isChecked) {
			    $(this).parent().prev('input').attr('type', 'text');
			    $(this).parent().addClass('active')
			  }
			  else {
			    $(this).parent().prev('input').attr('type', 'password'); 
			    $(this).parent().removeClass('active')
			  }
			});
		}
	});

	$("#filter-slider").slider({
		min: 15000,
		max: 95000,
		values: [25000,70000],
		range: true,
		stop: function(event, ui) {
			jQuery("input#minCost").val(jQuery("#filter-slider").slider("values",0));
			jQuery("input#maxCost").val(jQuery("#filter-slider").slider("values",1));
		},
		slide: function(event, ui){
			jQuery("input#minCost").val(jQuery("#filter-slider").slider("values",0));
			jQuery("input#maxCost").val(jQuery("#filter-slider").slider("values",1));
		}
	});

	//
	var endDate = "January 9, 2017 15:03:25";
    $('.countdown.styled').countdown({
      date: endDate,
      render: function(data) {
        $(this.el).html("<div>" + this.leadingZeros(data.days, 1) +"<span>д</span></div> : <div>" + this.leadingZeros(data.hours, 2) + "</div> : <div>" + this.leadingZeros(data.min, 2) + "</div> : <div>" + this.leadingZeros(data.sec, 2) + "</div>");
      }
    });

    $('.countdown.big').countdown({
      date: endDate,
      render: function(data) {
        $(this.el).html("<div><b>" + this.leadingZeros(data.days, 2) +"</b><span>дней</span></div> : <div><b>" + this.leadingZeros(data.hours, 2) + "</b><span>часов</span></div> : <div><b>" + this.leadingZeros(data.min, 2) + "</b><span>минут</span></div> : <div><b>" + this.leadingZeros(data.sec, 2) + "</b><span>секунд</span></div>");
      }
    });

    //
    $('.img-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows:false,
		fade: true,
		asNavFor: '.img-slider-nav'
	});
	$('.img-slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.img-slider',
		dots: false,
		arrows: true,
		focusOnSelect: true
	});

	//
	$('.scroll').mCustomScrollbar({
		axis:"y",
		scrollButtons:{enable:false},
		scrollInertia: 0
	});

	//
	$('.item-pane').addClass('hid');
	$('.item-pane:first-child').removeClass('hid');
	$('.item-tabs li:first-child').addClass('active');
	$('.item-tabs li a').click(function() {
	  var idx = $(this).parent().index();
	  $(this).parents('.item-info').find('.item-pane').not($('.item-pane').eq(idx)).addClass('hid');
	  $(this).parents('.item-info').find('.item-pane').eq(idx).removeClass('hid');
	  $('.item-tabs li').removeClass('active');
	  $(this).parent().addClass('active');
	  return false;
    });

    //.item-desc .btn
    $(".item-desc .btn").click(function(){
        var $el = $(this);
		$el.text($el.text() == "Свернуть" ? "Еще": "Свернуть");		
        $(this).parent().find('.text-sm').toggleClass('active');
        $(this).parent().find('.text-big').toggleClass('active');
        return false;
    });

});



